function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
    for(let key in defaultProps) {
        if (key in obj) {
            continue
        }
        else {
            obj[key] = defaultProps[key]
        }
    }
    return obj;
}

module.exports = defaults;