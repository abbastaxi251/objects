function mapObject(obj, cb=val=>val) {
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
    for(let key in obj) {
        if (typeof(obj[key]) == "number") {
            obj[key] = cb(obj[key]);
        }
    }
    return obj;
}

module.exports = mapObject;