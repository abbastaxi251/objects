function values(obj) {
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
    let value_list = [];
    for (let key in obj) {
        value_list.push(obj[key]);
    }
    return value_list;
}

module.exports = values;