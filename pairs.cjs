function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
    let pair_list = [];
    for(let key in obj) {
        pair_list.push([key,obj[key]]);
    }

    return pair_list;
}

module.exports = pairs;