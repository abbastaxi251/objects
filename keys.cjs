function keys(obj) {
    // Retrieve all the names of the object's properties.
    // Return the keys as strings in an array.
    // Based on http://underscorejs.org/#keys
    let key_list = [];
    for (let key in obj) {
        key_list.push(key);
    }
    return key_list;
}

module.exports = keys;